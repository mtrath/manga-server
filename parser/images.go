package parser

import (
	mangaio "bitbucket.org/mtrath/manga-server/io"
	"bytes"
	"encoding/xml"
)

type divImgHolder struct {
	Img img `xml:"a>img"`
}

type img struct {
	Url string `xml:"src,attr"`
}

type ImageUrl string

type ImageParser struct {
	BaseUrl    Url
	SiteUrl    SiteUrl
	oldSiteUrl SiteUrl
	imageUrl   ImageUrl
}

func NewImageParser(siteUrlSuffix SiteUrl) *ImageParser {
	parser := new(ImageParser)
	parser.BaseUrl = "http://www.mangareader.net"
	parser.SiteUrl = siteUrlSuffix
	return parser
}

func (this *ImageParser) openUrl() (*bytes.Reader, error) {
	path := string(this.BaseUrl) + string(this.SiteUrl)
	htmlContent, err := mangaio.OpenHTML(path)
	return bytes.NewReader(htmlContent), err
}

func (this *ImageParser) GetImageUrl() (ImageUrl, error) {
	if this.oldSiteUrl == this.SiteUrl {
		return this.imageUrl, nil
	}
	htmlFile, err := this.openUrl()
	if err != nil {
		return "", err
	}
	this.oldSiteUrl = this.SiteUrl

	parser := xml.NewDecoder(htmlFile)
	parser.Strict = false
	parser.AutoClose = xml.HTMLAutoClose
	parser.Entity = xml.HTMLEntity

	var imgXml xml.StartElement
	for {
		token, err := parser.Token()
		if err != nil {
			return this.imageUrl, nil
		}

		if startElement, ok := token.(xml.StartElement); ok &&
			startElement.Name.Local == "div" &&
			len(startElement.Attr) == 1 &&
			startElement.Attr[0].Value == "imgholder" {
			imgXml = startElement
			break
		}
	}

	var div divImgHolder
	parser.DecodeElement(&div, &imgXml)
	this.imageUrl = ImageUrl(div.Img.Url)

	return this.imageUrl, nil
}
