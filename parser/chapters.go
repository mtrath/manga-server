package parser

import (
	mangaio "bitbucket.org/mtrath/manga-server/io"
	"bytes"
	"encoding/xml"
	"errors"
	"strconv"
	"strings"
	"time"
)

type ChapterUrl string

type ChapterIndex int

type Chapter struct {
	Name string
	Url  ChapterUrl
	Date time.Time
}

type ChapterParser struct {
	MangaName   string
	BaseUrl     Url
	MangaUrl    MangaUrl
	oldMangaUrl MangaUrl
	chapters    []Chapter
}

func NewChapterParser(mangaUrlSuffix MangaUrl, mangaName string) *ChapterParser {
	parser := new(ChapterParser)
	parser.BaseUrl = "http://www.mangareader.net"
	parser.MangaUrl = mangaUrlSuffix
	parser.MangaName = mangaName
	return parser
}

func (this *ChapterParser) openUrl() (*bytes.Reader, error) {
	path := string(this.BaseUrl) + string(this.MangaUrl)
	htmlContent, err := mangaio.OpenHTML(path)
	return bytes.NewReader(htmlContent), err
}

func (this *ChapterParser) GetChaptersAndShortUrls() ([]Chapter, error) {
	if this.oldMangaUrl == this.MangaUrl {
		return this.chapters, nil
	}
	htmlFile, err := this.openUrl()
	this.chapters = make([]Chapter, 0, 300)
	if err != nil {
		return this.chapters, err
	}
	this.oldMangaUrl = this.MangaUrl

	parser := xml.NewDecoder(htmlFile)
	parser.Strict = false
	parser.AutoClose = xml.HTMLAutoClose
	parser.Entity = xml.HTMLEntity

	for {
		token, err := parser.RawToken()
		if err != nil {
			this.oldMangaUrl = ""
			return nil, err
		}

		if startElement, ok := token.(xml.StartElement); ok &&
			startElement.Name.Local == "table" &&
			len(startElement.Attr) == 1 &&
			startElement.Attr[0].Value == "listing" {
			break
		}
	}

	for {
		for {
			token, err := parser.RawToken()
			if err != nil {
				this.oldMangaUrl = ""
				return nil, err
			}

			if startElement, ok := token.(xml.StartElement); ok &&
				startElement.Name.Local == "div" &&
				len(startElement.Attr) == 1 {
				if startElement.Attr[0].Value == "chico_manga" {
					break
				}
				if startElement.Attr[0].Value == "clear" {
					// real exit point
					return this.chapters, nil
				}
			}
		}

		var name, url string
		var date string

		for {
			token, err := parser.RawToken()
			if err != nil {
				this.oldMangaUrl = ""
				return nil, err
			}

			if startElement, ok := token.(xml.StartElement); ok &&
				startElement.Name.Local == "a" {
				url = startElement.Attr[0].Value
				subToken, err := parser.RawToken()
				if err != nil {
					this.oldMangaUrl = ""
					return nil, err
				}
				name = string([]byte(subToken.(xml.CharData)))
				name = name[len(this.MangaName)+1:]
				break
			}
		}

		for {
			token, err := parser.RawToken()
			if err != nil {
				this.oldMangaUrl = ""
				return nil, err
			}

			if startElement, ok := token.(xml.StartElement); ok &&
				startElement.Name.Local == "td" {
				subToken, err := parser.RawToken()
				if err != nil {
					this.oldMangaUrl = ""
					return nil, err
				}
				date = string([]byte(subToken.(xml.CharData)))
				break
			}
		}
		newChapter := Chapter{name, ChapterUrl(url), getDate(date)}
		this.chapters = append(this.chapters, newChapter)
	}
}

func (this *ChapterParser) GetDate(ch ChapterIndex) (time.Time, error) {
	if len(this.chapters) == 0 {
		return time.Unix(0, 0), errors.New("The chapter list may " +
			"not have been initialized. Use " +
			"GetChaptersAndShortUrls() function first.")
	}
	for _, chapter := range this.chapters {
		if strings.Contains(chapter.Name, strconv.Itoa(int(ch))) {
			return chapter.Date, nil
		}
	}
	return time.Unix(0, 0), errors.New(
		"Chapter " + strconv.Itoa(int(ch)+1) + " not found in map.")
}

func (this *ChapterParser) GetName(ch ChapterIndex) (string, error) {
	if len(this.chapters) == 0 {
		return "", errors.New("The chapter list may not have been " +
			"initialized. Use GetChaptersAndShortUrls() " +
			"function first.")
	}
	for _, chapter := range this.chapters {
		if strings.Contains(chapter.Name, strconv.Itoa(int(ch))) {
			return chapter.Name, nil
		}
	}
	return "", errors.New(
		"Chapter " + strconv.Itoa(int(ch)) + " not found in map.")
}

func (this *ChapterParser) GetUrl(ch ChapterIndex) (ChapterUrl, error) {
	if len(this.chapters) == 0 {
		return ChapterUrl(""), errors.New(
			"The chapter list may not have been " +
				"initialized. Use GetChaptersAndShortUrls() " +
				"function first.")
	}
	for _, chapter := range this.chapters {
		if strings.Contains(chapter.Name, strconv.Itoa(int(ch))) {
			return chapter.Url, nil
		}
	}
	return ChapterUrl(""), errors.New(
		"Chapter " + strconv.Itoa(int(ch)) + " not found in map.")
}

func decIndex(ch ChapterIndex) ChapterIndex {
	return ChapterIndex(int(ch) - 1)
}

func getDate(date string) time.Time {
	values := strings.Split(date, "/")

	year, _ := strconv.Atoi(values[2])
	month, _ := strconv.Atoi(values[0])
	day, _ := strconv.Atoi(values[1])

	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
}
