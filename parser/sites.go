package parser

import (
	mangaio "bitbucket.org/mtrath/manga-server/io"
	"bytes"
	"encoding/xml"
	"io"
)

type divSelectSite struct {
	Sites []option `xml:"select>option"`
}

type option struct {
	Url string `xml:"value,attr"`
}

type SiteNum int

type SiteUrl string

type Site struct {
	Num SiteNum
	Url SiteUrl
}

type SiteParser struct {
	BaseUrl       Url
	ChapterUrl    ChapterUrl
	oldChapterUrl ChapterUrl
	sites         []Site
}

func NewSiteParser(chapterUrlSuffix ChapterUrl) *SiteParser {
	parser := new(SiteParser)
	parser.BaseUrl = "http://www.mangareader.net"
	parser.ChapterUrl = chapterUrlSuffix
	return parser
}

func (this *SiteParser) openUrl() (io.Reader, error) {
	path := string(this.BaseUrl) + string(this.ChapterUrl)
	htmlContent, err := mangaio.OpenHTML(path)
	return bytes.NewReader(htmlContent), err
}

func (this *SiteParser) GetSitesShortUrls() ([]Site, error) {
	if this.oldChapterUrl == this.ChapterUrl {
		return this.sites, nil
	}
	this.sites = make([]Site, 0, 35)
	htmlFile, err := this.openUrl()
	if err != nil {
		return this.sites, err
	}
	this.oldChapterUrl = this.ChapterUrl

	parser := xml.NewDecoder(htmlFile)
	parser.Strict = false
	parser.AutoClose = xml.HTMLAutoClose
	parser.Entity = xml.HTMLEntity

	var siteSelectorDiv xml.StartElement
	for {
		token, err := parser.Token()
		if err != nil {
			return this.sites, nil
		}

		if startElement, ok := token.(xml.StartElement); ok &&
			startElement.Name.Local == "div" &&
			len(startElement.Attr) == 1 &&
			startElement.Attr[0].Value == "selectpage" {
			siteSelectorDiv = startElement
			break
		}
	}

	var sites divSelectSite
	parser.DecodeElement(&sites, &siteSelectorDiv)
	for i, page := range sites.Sites {
		this.sites = append(
			this.sites, Site{SiteNum(i + 1), SiteUrl(page.Url)})
	}

	return this.sites, nil
}
