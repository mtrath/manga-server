# Manga-Server

Manga-Server is a server application which downloads and compresses manga images from [www.mangareader.net]. The images are compressed into a single zip file and are send to a client via http, thus it works in a browser.

Clients for linux and android are planned.

Available URLs are:

- http://serverip/manga/list
- http://serverip/manga/ch/Bleach
- http://serverip/manga/cover/Bleach
- http://serverip/manga/get/Bleach/100.zip  
where Bleach is an arbitrary manga and 100 an arbitrary chapter number.

## Standalone

You may start Manga-Server on a terminal with `./manga-server`. By default Manga-Server will save all files in an "archive" subdirectory inside the working dir. This program is written in Go, but it's not needed at runtime.

See `./manga-server -h` for information about command line options.


## Integration with Apache HTTP Server

Let's assume Manga-Server is compiled to listen for `:8080/manga`.

Open your Apache config file and insert a new proxy pass:  
(probably `/etc/httpd/conf/httpd.conf`, on Ubuntu `/etc/apache2/apache2.conf`,  
or see your distribution's documentation or package contents)

	ProxyPass /manga http://127.0.0.1:8080/manga
	ProxyPassReverse /manga http://127.0.0.1:8080/manga

Save it, restart Apache and start Manga-Server.

For this to work the `proxy_module` module must be loaded. See [Apache documentation] for more information.


## TODO

A software project is never finished, so there are plenty things to do:

- implementing a startup script which manages setting up the commandline option (reading a config file)
- should delete zip files older than a specific date (e.g. 7 days)

[www.mangareader.net]: http://www.mangareader.net
[Apache documentation]: http://httpd.apache.org/docs/2.2/mod/mod_proxy.html